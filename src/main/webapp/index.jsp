<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<style>
	body {
		text-align: left;
	}
	
	ul>li {
		padding: 8px;
	}
</style>

<body>

<p align="center">
<h2>MPSDK4J-V2 Demo</h2>
</p>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<ul>
<li>Servlet入口: <%=basePath%>/servlet/wechatcore.ser</li>
<li>SpringMVC入口: <%=basePath%>/springmvc/wechatcore.do</li>
<li>Nutz入口: <%=basePath%>/nutz/wechatcore.ntz</li>
</ul>

</body>
</html>
