package io.github.elkan1788.mpsdk4jdemo.nutz;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;

import io.github.elkan1788.mpsdk4j.core.WechatDefHandler;
import io.github.elkan1788.mpsdk4j.mvc.WechatWebSupport;
import io.github.elkan1788.mpsdk4j.util.ConfigReader;
import io.github.elkan1788.mpsdk4j.vo.MPAccount;

/**
 * Nutz环境接入
 * 
 * @author 凡梦星尘(elkan1788@gmail.com)
 * @since 2.0
 */
@At("/nutz")
@IocBean
public class WechatCoreModule extends WechatWebSupport {

    private static final Log log = Logs.get();

    private static final ConfigReader _cr = new ConfigReader("/mp.properties");

    @Override
    public void init() {
        log.info("====== Nutz环境 =======");
        super.init();
        MPAccount mpact = new MPAccount();
        mpact.setMpId(_cr.get("mpId"));
        mpact.setAppId(_cr.get("appId"));
        mpact.setAppSecret(_cr.get("appSecret"));
        mpact.setToken(_cr.get("token"));
        mpact.setAESKey(_cr.get("aesKey"));
        _wk.setMpAct(mpact);
        _wk.setWechatHandler(new WechatDefHandler());
    }

    @At("/wechatcore")
    public void wechatCore(HttpServletRequest req, HttpServletResponse resp) {
        try {
            this.interact(req, resp);
        }
        catch (IOException e) {
            throw Lang.wrapThrow(e);
        }
    }
}
